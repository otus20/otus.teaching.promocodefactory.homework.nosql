﻿using System.Threading.Tasks;
using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Repositories
{
    public class PromoCodeRepository : MongoRepository<PromoCode>
    {
        private readonly IRepository<PromoCodeCustomer> _promoCodeCustomerRepository;
        //private readonly IMongoClient _client;

        public PromoCodeRepository(IMongoDatabase mongoDb, IRepository<PromoCodeCustomer> promoCodeCustomerRepository)
            : base(mongoDb)
        {
            //_client = mongoDb.Client;
            _promoCodeCustomerRepository = promoCodeCustomerRepository;
        }

        public override async Task AddAsync(PromoCode entity)
        {
            await Collection.InsertOneAsync(entity);
            await _promoCodeCustomerRepository.AddManyAsync(entity.Customers);
        }

        // Error: Standalone servers do not support transactions.
        // In order to perform multi-document transactions,
        // your MongoDB deployment needs to be either a replica set or a sharded cluster

        // public override async Task AddAsync(PromoCode entity)
        // {
        //     using var session = await _client.StartSessionAsync();
        //
        //     session.StartTransaction();
        //
        //     await Collection.InsertOneAsync(session, entity);
        //     await _promoCodeCustomerRepository.AddManyAsync(session, entity.Customers);
        //
        //     await session.CommitTransactionAsync();
        // }
    }
}
