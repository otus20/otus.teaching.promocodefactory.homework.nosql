﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{
    public class MongoDbInitializer
        : IDbInitializer
    {
       private readonly IMongoCollection<Preference> _preferenceCollection;
       private readonly IMongoCollection<Customer> _customerCollection;

        public MongoDbInitializer(IMongoDatabase mongoDb)
        {
           _preferenceCollection = mongoDb.GetCollection<Preference>(nameof(Preference));
           _customerCollection =  mongoDb.GetCollection<Customer>(nameof(Customer));
        }
        
        public void InitializeDb()
        {
           _preferenceCollection.DeleteMany(e => true);
           _preferenceCollection.InsertMany(FakeDataFactory.Preferences);
           
           _customerCollection.DeleteMany(e => true);
           _customerCollection.InsertMany(FakeDataFactory.Customers);
        }
    }
}