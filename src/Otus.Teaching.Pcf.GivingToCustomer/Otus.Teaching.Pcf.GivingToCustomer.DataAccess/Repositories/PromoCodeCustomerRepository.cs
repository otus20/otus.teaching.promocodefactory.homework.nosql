﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Repositories
{
    public class PromoCodeCustomerRepository : MongoRepository<PromoCodeCustomer>
    {
        private readonly IMongoCollection<PromoCode> _promoCodeCollection;

        public PromoCodeCustomerRepository(IMongoDatabase mongoDb)
            : base(mongoDb)
        {
            _promoCodeCollection = mongoDb.GetCollection<PromoCode>(nameof(PromoCode));
        }

        public override async Task<List<PromoCodeCustomer>> GetWhere(
            Expression<Func<PromoCodeCustomer, bool>> predicate)
        {
            var promoCodeCustomerItems = await Collection
                .AsQueryable()
                .Where(predicate)
                .ToListAsync();

            var itemGroups = promoCodeCustomerItems.ToLookup(x => x.PromoCodeId)
                .ToDictionary(x => x.Key, x => x.ToList());

            var promoCodes = await _promoCodeCollection
                .AsQueryable()
                .Where(x => itemGroups.Keys.Contains(x.Id))
                .ToListAsync();

            promoCodes.ForEach(p =>
                itemGroups[p.Id].ForEach(x => x.PromoCode = p));

            return promoCodeCustomerItems;
        }
    }
}
