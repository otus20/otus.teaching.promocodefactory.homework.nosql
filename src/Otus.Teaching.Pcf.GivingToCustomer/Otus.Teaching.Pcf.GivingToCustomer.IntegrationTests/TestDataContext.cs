﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess;

namespace Otus.Teaching.Pcf.GivingToCustomer.IntegrationTests
{
    public class TestDataContext
        : DataContext
    {
        public TestDataContext()
        {
        }

        public TestDataContext(DbContextOptions<TestDataContext> options)
            : base(new DbContextOptions<DataContext>(options.Extensions.ToDictionary(p => p.GetType(), p => p)))
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Filename=PromocodeFactoryGivingToCustomerDb.sqlite");
        }
    }
}
