﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Repositories
{
   public class EmployeeRepository: IRepository<Employee>
   {
         private readonly IMongoCollection<Employee> _employeeCollection;

         public EmployeeRepository(IMongoCollection<Employee> employeeCollection)
         {
            _employeeCollection = employeeCollection;
         }

         public async Task<IEnumerable<Employee>> GetAllAsync()
         {
            return await _employeeCollection.AsQueryable().ToListAsync();
         }

         public Task<Employee> GetByIdAsync(Guid id)
         {
            var filter = Builders<Employee>.Filter.Eq(doc => doc.Id, id);
            return _employeeCollection.Find(filter).SingleOrDefaultAsync();
         }

         public Task<IEnumerable<Employee>> GetRangeByIdsAsync(List<Guid> ids)
         {
            throw new NotImplementedException();
         }

         public Task<Employee> GetFirstWhere(Expression<Func<Employee, bool>> predicate)
         {
            throw new NotImplementedException();
         }

         public Task<IEnumerable<Employee>> GetWhere(Expression<Func<Employee, bool>> predicate)
         {
            throw new NotImplementedException();
         }

         public Task AddAsync(Employee entity)
         {
            throw new NotImplementedException();
         }

         public async Task UpdateAsync(Employee entity)
         {
            var filter = Builders<Employee>.Filter.Eq(doc => doc.Id, entity.Id);
            await _employeeCollection.FindOneAndReplaceAsync(filter, entity);
         }

         public Task DeleteAsync(Employee entity)
         {
            throw new NotImplementedException();
         }
   }
}
