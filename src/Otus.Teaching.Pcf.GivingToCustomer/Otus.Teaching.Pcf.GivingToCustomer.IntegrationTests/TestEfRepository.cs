﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Repositories;

namespace Otus.Teaching.Pcf.GivingToCustomer.IntegrationTests
{
    public class TestEfRepository<T> : EfRepository<T>
        where T: BaseEntity
    {
        public TestEfRepository(TestDataContext dataContext) : base(dataContext)
        {
        }
    }
}
