﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{
    public class MongoDbInitializer
        : IDbInitializer
    {
       private readonly IMongoCollection<Employee> _employeeCollection;

        public MongoDbInitializer(IMongoCollection<Employee> employeeCollection)
        {
           _employeeCollection = employeeCollection;
        }
        
        public void InitializeDb()
        {
            _employeeCollection.DeleteMany(e => true);
            _employeeCollection.InsertMany(FakeDataFactory.Employees);
        }
    }
}