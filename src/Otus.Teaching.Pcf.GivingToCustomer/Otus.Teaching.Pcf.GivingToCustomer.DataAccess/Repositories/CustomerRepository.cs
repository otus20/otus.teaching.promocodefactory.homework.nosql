﻿using System;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Repositories
{
    public class CustomerRepository : MongoRepository<Customer>
    {
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<PromoCodeCustomer> _promoCodeCustomerRepository;

        public CustomerRepository(
            IMongoDatabase mongoDb,
            IRepository<Preference> preferenceRepository,
            IRepository<PromoCodeCustomer> promoCodeCustomerRepository)
            : base(mongoDb)
        {
            _preferenceRepository = preferenceRepository;
            _promoCodeCustomerRepository = promoCodeCustomerRepository;
        }

        public override async Task<Customer> GetByIdAsync(Guid id)
        {
            var customer = await base.GetByIdAsync(id);
            var preferences = await _preferenceRepository.GetRangeByIdsAsync(customer.PreferenceIds.ToList());

            customer.Preferences = preferences.Select(x => new CustomerPreference()
            {
                CustomerId = customer.Id,
                Preference = x,
                PreferenceId = x.Id
            }).ToList();

            customer.PromoCodes = await _promoCodeCustomerRepository.GetWhere(x => x.CustomerId == id);

            return customer;
        }
    }
}
