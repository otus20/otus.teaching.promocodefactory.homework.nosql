﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.Integration;
using Otus.Teaching.Pcf.GivingToCustomer.IntegrationTests.Data;

namespace Otus.Teaching.Pcf.GivingToCustomer.IntegrationTests
{
    public class TestWebApplicationFactory<TStartup>
        : WebApplicationFactory<TStartup> where TStartup: class
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureServices(services =>
            {
                var mongoServices = new HashSet<Type>
                {
                    typeof(IRepository<Customer>),
                    typeof(IRepository<Preference>)
                };

                var serviceDescriptorsToRemove = services
                    .Where(d => mongoServices.Contains(d.ServiceType))
                    .ToList();

                serviceDescriptorsToRemove.ForEach(x => services.Remove(x));

                services.AddScoped<INotificationGateway, NotificationGateway>();

                services.AddDbContext<TestDataContext>(x =>
                {
                    x.UseSqlite("Filename=PromoCodeFactoryDb.sqlite");
                    //x.UseNpgsql(Configuration.GetConnectionString("PromoCodeFactoryDb"));
                    //x.UseSnakeCaseNamingConvention();
                    x.UseLazyLoadingProxies();
                });

                services.AddScoped(typeof(IRepository<Customer>), typeof(TestEfRepository<Customer>));
                services.AddScoped(typeof(IRepository<Preference>), typeof(TestEfRepository<Preference>));

                var sp = services.BuildServiceProvider();

                using var scope = sp.CreateScope();
                var scopedServices = scope.ServiceProvider;
                var dbContext = scopedServices.GetRequiredService<TestDataContext>();
                var logger = scopedServices
                    .GetRequiredService<ILogger<TestWebApplicationFactory<TStartup>>>();

                try
                {
                    new EfTestDbInitializer(dbContext).InitializeDb();
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, "Проблема во время заполнения тестовой базы. " +
                                        "Ошибка: {Message}", ex.Message);
                }
            });
        }
    }
}
